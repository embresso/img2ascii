extern crate ansi_term;
extern crate clap;
extern crate image;

use ansi_term::Colour::RGB;
use ansi_term::{ANSIString, ANSIStrings};
use clap::{App, Arg};
use image::{open, GenericImageView};

// conversion table
static ASCIICHAR: [char; 10] = [' ', '.', ':', '-', '=', '+', '*', '#', '%', '@'];

fn convert2ascii(pxl_value: u8) -> char {
    // convert the grey level (0 to 255) into a 10-scale char
    let mut idx: usize = (pxl_value as usize * ASCIICHAR.len()) / 255;
    if idx != 0 {
        idx -= 1;
    }
    ASCIICHAR[idx]
}

fn colorize(img: &image::DynamicImage, ascii_img: Vec<char>) -> Vec<ANSIString<'static>> {
    // get the rgb colors
    let img_rgb = img.to_rgb();

    // Vec containing colored ANSIString
    let mut color_string: Vec<ANSIString<'static>> = Vec::new();

    // colorization
    for (i, pxl) in img_rgb.pixels().enumerate() {
        color_string.push(RGB(pxl.0[0], pxl.0[1], pxl.0[2]).paint(ascii_img[i].to_string()));
        // insert new line when arriving at the end of the row
        if i % img.dimensions().0 as usize == 0 {
            color_string.push(ANSIString::from("\n"));
        }
    }
    color_string
}

fn main() {
    let matches = App::new("img2ascii")
        .version("2.0.0")
        .author("Emmanuel Bresso")
        .about("Convert an image into ascii art")
        .arg(
            Arg::with_name("image_file")
                .short("i")
                .long("image_file")
                .value_name("image_file")
                .help("image to convert")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("resize")
                .short("r")
                .long("resize")
                .value_name("resize_factor")
                .help("Resize factor")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("color")
                .short("c")
                .long("color")
                .help("Colorized output"),
        )
        .get_matches();

    // Open image
    let path = matches.value_of("image_file").unwrap();
    let mut img = match open(path) {
        Ok(i) => i,
        Err(e) => panic!("Cannot open file {}, err: {}", path, e),
    };

    let color: bool = matches.is_present("color");

    if matches.is_present("resize") {
        let resize_factor: u32 = matches.value_of("resize").unwrap().parse().unwrap();
        println!("{}", img.dimensions().0);
        img = img.resize_exact(
            img.dimensions().0 / resize_factor,
            img.dimensions().1 / resize_factor,
            image::FilterType::Lanczos3,
        );
        println!("{}", img.dimensions().0);
    }

    // convert into greyscale
    println!("{}", img.dimensions().0);
    let img_luma = img.to_luma();
    let mut ascii_img: Vec<char> = Vec::new();

    // transform each pixel into ascii
    for pxl in img_luma.pixels() {
        ascii_img.push(convert2ascii(pxl[0]));
    }

    // Colorize
    if color {
        let color_string = colorize(&img, ascii_img);
        let to_print = ANSIStrings(&color_string);
        println!("{}", to_print);
    } else {
        for chunk in ascii_img.chunks(img.dimensions().0 as usize) {
            for character in chunk {
                print!("{}", character);
            }
            println!();
        }
    }
}
