# img2ascii 2.0.0
Convert an image into ascii art

## USAGE:
    img2ascii --image_file <image_file>

## FLAGS:
    -c, --color      Colorized output
    -h, --help       Prints help information
    -V, --version    Prints version information

## OPTIONS:
    -i, --image_file <image_file>    image to convert
    -r, --resize <resize_factor>     Resize factor